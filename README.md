# Ejercicio N° 6 -  Design Patterns
### Técnicas y Herramientas 2020 | Maestría en Ingeniería de Software | Universidad Nacional de La Plata

### Instalación

El script requiere [Node.js](https://nodejs.org/) v13+.
Instalar dependencias

```sh
$ cd tecnicas_herramientas_ejercicio_6
$ npm install -d
$ npm run test
```
### Configuración jest
Se encuentra configurado en el package.json para que emita el reporte en formato HTML.

### Dependencias
Las siguientes dependencias son necesarias para el proyecto.

| Dependencia | README |
| ------ | ------ |
| jest | [https://jestjs.io/][jest] |

  [node.js]: <http://nodejs.org>
  [jest]: <https://jestjs.io/>


### Patrones aplicados en el Proyecto

- State (Behaivoral Pattern)
- Strategy (Behaivoral Pattern)