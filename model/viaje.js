const StrategyViajeConductor = require("../strategy/StrategyViajeConductor");
const StrategyViajePasajero = require("../strategy/StrategyViajePasajero");

class Viaje{

	constructor(costo, vehiculo, origen, destino){
		this.fecha = new Date();
		this.costo = costo;
        this.vehiculo = vehiculo;
        this.origen = origen;
        this.destino = destino;
        this.pasajeros = [];
		this.finalizado = false;
		this.conductor().agregarViaje(this);
    }

        hayLugar(){
            return this.vehiculo.getCapacidad() > this.pasajeros.length;
        }

    agregarPasajero(pasajero){
        if (this.hayLugar() & !this.finalizado){
			pasajero.agregarViaje(this);
			this.pasajeros.push(pasajero);

            return true;
        }
        return false;
    }

    calcularCosto(){
        return this.costo / (this.pasajeros.length + 1);
    }

    conductor(){
        return this.vehiculo.getPropietario();
    }

	getRandomPasajeroPremiado(){
		 return this.pasajeros[Math.floor(Math.random() * this.pasajeros.length)];
	}

    finalizarViaje(){
        if (!this.finalizado){
            let costo = this.calcularCosto();
            let strategyViajeConductor = new StrategyViajeConductor();
            let strategyViajePasajero = new StrategyViajePasajero();
			let pasajeroPremiado = this.getRandomPasajeroPremiado();

			strategyViajeConductor.finalizarViaje(this.conductor(),costo);

			this.pasajeros.forEach(  pasajero => {
				if (pasajero.getDni() != pasajeroPremiado.getDni() )
						strategyViajePasajero.finalizarViaje(pasajero,costo);
            });

			this.finalizado = true;
        }
	}

	setDate(date){
		this.fecha= new Date(date);
	}
}

module.exports = Viaje;
