const UsuarioNormal = require("./tipousuario/UsuarioNormal");

class Usuario{

    constructor(nombre, dni){
        this.creditos = 1000;
		this.nombre = nombre;
		this.dni = dni;
		this.viajes = [];
		this.tipo = new UsuarioNormal();
	}

	setTipo(tipoUsuario){
		this.tipo = tipoUsuario;
	}

	getTipo(){
		return this.tipo;
	}

	getDni(){
		return this.dni;
	}
	getCreditos(){
		return this.creditos;
	}

    sumarCreditos(cantidad){
        this.creditos = this.creditos + cantidad;
    }

   	agregarViaje(viaje){
		this.viajes.push(viaje);
	}

	getUltimoViaje(){
		return this.viajes.reduce((maxValue, minValue) => (maxValue.fecha > minValue.fecha) ? maxValue : minValue);
	}

	getUltimoViajeConductor(){
		return this.getViajesComoConductorCompletados().reduce((maxValue, minValue) => (maxValue.fecha > minValue.fecha) ? maxValue : minValue);
	}

	getDiferenciaDiasUltimoViajeConductor(){
		return Math.round(Math.abs((new Date() - this.getUltimoViajeConductor().fecha) / (1000 * 3600 * 24)));
	}

	getViajesComoConductorCompletados(){
		return this.viajes.filter(viaje => {
			return ((viaje.conductor().dni == this.dni) && (viaje.finalizado==true))
		})
	}

}

module.exports = Usuario;
