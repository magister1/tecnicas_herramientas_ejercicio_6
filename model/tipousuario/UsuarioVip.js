const UsuarioNormal = require("./UsuarioNormal");
const UsuarioTipo = require("./UsuarioTipo");

class UsuarioVip extends UsuarioTipo{


	descontarCreditos(usuario,costo){
		let descuento = costo * 0.10;
		usuario.creditos = usuario.creditos -  (costo - descuento) ;
		this.verificarUsuarioTipo(usuario);
	}

	verificarUsuarioTipo(usuario){
		if ((usuario.getViajesComoConductorCompletados().length > 50 ) && (usuario.getDiferenciaDiasUltimoViajeConductor() > 30)){
            usuario.setTipo(new UsuarioNormal());
        }
    }

}


module.exports = UsuarioVip;