const UsuarioVip = require("./UsuarioVip");
const UsuarioTipo = require("./UsuarioTipo");

class UsuarioNormal extends UsuarioTipo{


	descontarCreditos(usuario,costo){
		usuario.creditos = usuario.creditos - costo ;
		this.verificarUsuarioTipo(usuario);
	}

	verificarUsuarioTipo(usuario){
		if ((usuario.getViajesComoConductorCompletados().length > 50 ) && (usuario.getDiferenciaDiasUltimoViajeConductor() < 30)){
			usuario.setTipo(new UsuarioVip());
		}
	}

}


module.exports = UsuarioNormal;