const StrategyViaje = require("./StrategyViaje");

class StrategyViajePasajero  extends StrategyViaje{

    finalizarViaje(usuario, costo){
        usuario.getTipo().descontarCreditos(usuario, costo);
    }

}

module.exports = StrategyViajePasajero;