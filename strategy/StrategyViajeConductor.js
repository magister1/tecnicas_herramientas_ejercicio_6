const StrategyViaje = require("./StrategyViaje");


class StrategyViajeConductor  extends StrategyViaje{

    finalizarViaje(usuario, costo){
        usuario.sumarCreditos(costo * 0.20);
        usuario.getTipo().descontarCreditos(usuario, costo);
    }

}

module.exports = StrategyViajeConductor;