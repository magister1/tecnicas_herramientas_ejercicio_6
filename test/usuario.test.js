const Usuario = require('../model/usuario.js');
const Vehiculo = require('../model/vehiculo.js');
const Viaje = require('../model/viaje.js');
const UsuarioNormal = require('../model/tipousuario/UsuarioNormal.js');
const UsuarioVip = require('../model/tipousuario/UsuarioVip.js');

let creditos=null;

beforeEach(() => {
	creditos=100;
});

test('Constructor', () => {

	let usuario =  new Usuario("Juan",12345678);
	expect(usuario.creditos).toBe(1000);

});

test('Descontar Créditos', () => {

	let usuario = new  Usuario("Pedro", 34123456);
	usuario.getTipo().descontarCreditos(usuario, creditos);
	expect(usuario.creditos).toBe(900);

});

test('Sumar Créditos', () => {

	let usuario = new  Usuario("Pedro",34123456);
	usuario.sumarCreditos(creditos)
	expect(usuario.creditos).toBe(1100);

});


describe('Testeamos los usuarios con viajes completados', () => {

	let origen,destino = null;
	let usuarioFabian,usuarioJuan, vehiculoFabian,vehiculoJuan = null;

	beforeEach(() => {

		origen = "La Plata";
		destino = "Ensenada";

		usuarioJuan = new Usuario("Juan", 11111111);
		usuarioJavier = new Usuario("Javier", 55555555);
		usuarioFabian = new Usuario("Fabian", 22222222);
		vehiculoJuan = new Vehiculo(4,"Chevrolet Onix", usuarioJuan);
		vehiculoFabian = new Vehiculo(4,"Chevrolet Cruze", usuarioFabian);
	});

	test('Testeamos cantidad de viajes como conductor  de un usuario', () => {
		let cantidadViajes = 10;

		for (let index = 0; index < cantidadViajes; index++) {
			let viaje = new Viaje(100,vehiculoJuan,origen,destino);
			viaje.agregarPasajero(usuarioJavier);
			viaje.agregarPasajero(usuarioFabian);
			viaje.finalizarViaje();
		}

		let viaje = new Viaje(100,vehiculoFabian,origen,destino);
		viaje.agregarPasajero(usuarioJavier);
		viaje.finalizarViaje();

		expect(usuarioFabian.getViajesComoConductorCompletados().length).toBe(1);
		expect(usuarioJuan.getViajesComoConductorCompletados().length).toBe(cantidadViajes);
	});

	test('Tipo de usuario segun las condiciones para ser NORMAL', () => {

		let cantidadViajes = 30;
		var twoWeeksAgoDate= new Date( Date.now() - 14 * 24 * 60 * 60 * 1000)  ;
		var twoMonthsAgoDate= new Date( Date.now() - 60 * 24 * 60 * 60 * 1000)  ;

		for (let index = 0; index < cantidadViajes; index++) {
			let viaje = new Viaje(100,vehiculoJuan,origen,destino);
			viaje.setDate(twoMonthsAgoDate);
			viaje.agregarPasajero(usuarioJavier);
			viaje.agregarPasajero(usuarioFabian);
			viaje.finalizarViaje();
		}
		expect(usuarioJuan.getTipo() instanceof UsuarioNormal).toBeTruthy()

		for (let index = 0; index < cantidadViajes; index++) {
			let viaje = new Viaje(100,vehiculoJuan,origen,destino);
			viaje.setDate(twoMonthsAgoDate);
			viaje.agregarPasajero(usuarioJavier);
			viaje.agregarPasajero(usuarioFabian);
			viaje.finalizarViaje();
		}

		for (let index = 0; index < 3; index++) {
			let viaje = new Viaje(100,vehiculoJuan,origen,destino);
			viaje.agregarPasajero(usuarioJavier);
			viaje.agregarPasajero(usuarioFabian);
			viaje.finalizarViaje();
		}

		expect(usuarioJuan.getTipo() instanceof UsuarioVip).toBeTruthy()

	});
});

