const Usuario = require('../model/usuario.js');
const Vehiculo = require('../model/vehiculo.js');
const Viaje = require('../model/viaje.js');


let origen,destino,usuarioJuan, vehiculoJuan = null;

beforeEach(() => {

	origen = "La Plata";
	destino = "Ensenada";
	usuarioJuan = new Usuario("Juan", 11111111);
	vehiculoJuan = new Vehiculo(4,"Chevrolet Onix", usuarioJuan);

});

test('Constructor', () => {

	let viaje = new Viaje(100,vehiculoJuan,origen,destino);
	expect(viaje.costo).toBe(100);
	expect(viaje.pasajeros.length).toBe(0);
	expect(viaje.finalizado).toBeFalsy();
	expect(viaje.origen == origen).toBeTruthy();
	expect(viaje.destino == destino).toBeTruthy();
	expect(viaje.vehiculo  instanceof Vehiculo).toBeTruthy()

});


test('Se verifica que haya lugar disponible al crear un viaje nuevo', () => {
	let viaje = new Viaje(1000,vehiculoJuan,origen,destino);
	expect(viaje.hayLugar()).toBeTruthy();
});


test('Test conductor del vehiculo sea el propietario', () => {

	let viaje = new Viaje(1000,vehiculoJuan,origen,destino);
	expect(viaje.conductor() != undefined).toBeTruthy();
	expect(viaje.conductor() instanceof Usuario).toBeTruthy();
	expect(viaje.conductor()).toBe(usuarioJuan);

});

test('Calcular costo sin pasajeros, solo conductor', () => {

	let viaje = new Viaje(1000,vehiculoJuan,origen,destino);
	expect(viaje.calcularCosto()).toBe(1000);

});


describe('Testeamos un viaje completo', () => {

	let origen,destino = null;
	let usuarioAgustin,usuarioFabian,usuarioMarcelo,usuarioJuan, vehiculoFabian,vehiculoJuan = null;

	beforeEach(() => {

		origen = "La Plata";
		destino = "Ensenada";

		usuarioJuan = new Usuario("Juan", 11111111);
		usuarioJavier = new Usuario("Javier", 55555555);
		usuarioFabian = new Usuario("Fabian", 22222222);
		usuarioMarcelo = new Usuario("Marcelo", 33333333);
		usuarioAgustin = new Usuario("Agustin", 44444444);

		vehiculoJuan = new Vehiculo(4,"Chevrolet Onix", usuarioJuan);
		vehiculoFabian = new Vehiculo(4,"Chevrolet Cruze", usuarioFabian);

	});

	test('Testeamos los descuentos de un viaje con tipo usuario normal ', () => {

		let viaje = new Viaje(100,vehiculoJuan,origen,destino);
		viaje.agregarPasajero(usuarioJavier);
		viaje.agregarPasajero(usuarioAgustin);
		viaje.agregarPasajero(usuarioFabian);
		viaje.finalizarViaje();

		let cantPasajerosConDescuento = (viaje.pasajeros.filter(pasajero => pasajero.getCreditos() == 975)).length;
		let cantPasajerosPremiados =    (viaje.pasajeros.filter(pasajero => pasajero.getCreditos() == 1000)).length;
		expect(cantPasajerosPremiados).toBe(1);
		expect(cantPasajerosConDescuento).toBe(2);
		expect(viaje.conductor().getCreditos()).toBe(980);

	});


	test('Testeamos los descuentos de acuerdo a un tipo de usuario ', () => {

		let viaje = new Viaje(100,vehiculoJuan,origen,destino);
		viaje.agregarPasajero(usuarioJavier);
		viaje.agregarPasajero(usuarioAgustin);
		viaje.agregarPasajero(usuarioFabian);
		viaje.finalizarViaje();

		let cantPasajerosConDescuento = (viaje.pasajeros.filter(pasajero => pasajero.getCreditos() == 975)).length;
		let cantPasajerosPremiados =    (viaje.pasajeros.filter(pasajero => pasajero.getCreditos() == 1000)).length;
		expect(cantPasajerosPremiados).toBe(1);
		expect(cantPasajerosConDescuento).toBe(2);
		expect(viaje.conductor().getCreditos()).toBe(980);

	});




});

/*
describe('Testeamos un viaje completo', () => {
	let viaje = null;
	let vehiculo = null;
	let propietario = null;

	beforeEach(() => {
		//Creamos un viaje con tl total completo de pasajeros
		vehiculo = new Vehiculo(4,"Chevrolet Cruze", propietario);
		viaje = new Viaje(1000,vehiculo,origen,destino);
		propietario = new Usuario("Juan",12345678);
		for (let index = 0; index < vehiculo.capacidad; index++) {
			viaje.agregarPasajero(new Usuario(nombres[Math.floor(Math.random() * nombres.length)]));
		}
	})

	test('Verficamos que no permita agregar un pasajero si está completo ', () => {
		let agregado = viaje.agregarPasajero(new Usuario(nombres[Math.floor(Math.random() * nombres.length)]));
		expect(agregado).toBeFalsy();
		expect(viaje.calcularCosto()).toBe(200);//Costo con un pasajero

	});

	test('Testeamos finalizar viaje', () => {

		viaje.finalizarViaje();
		expect(viaje.finalizado).toBeTruthy();
		expect(viaje.conductor().creditos).toBe(800); //Verificamos creditos del conductor

		viaje.pasajeros.forEach(p => {
			expect(p.creditos).toBe(800); //Verificamos creditos de los pasajeros
		});

	});
});

*/

